import React, {Fragment} from 'react';
import {BrowserRouter as Router, Route, Switch, Redirect} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "./components/layouts/Header";
import "./App.css";
import {Container} from "react-bootstrap";
import Shortener from "./components/shortener/Shortener";
import {Provider} from "react-redux";
import store from "./store";
import Urls from "./components/urls/Urls";

function App() {
    return (
        <Provider store={store}>
            <Router>
                <Fragment>
                    <Header/>
                    <Container>
                        <Switch>
                            <Redirect exact from="/" to="/shortener"/>
                            <Route exact path="/shortener" component={Shortener}/>
                            <Route exact path="/list-urls" component={Urls}/>
                        </Switch>
                    </Container>
                </Fragment>
            </Router>
        </Provider>
    );
}

export default App;
