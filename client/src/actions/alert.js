import uuid from 'uuid';
import {SET_ALERT} from './types';

export const setAlert = (errors, timeout = 3000) => dispatch => {

    let errorsToDisplay = errors.map(e => {
        let errObj = {
            error: e, id: uuid.v4()
        };
        return errObj;
    });

    dispatch({
        type: SET_ALERT,
        payload: errorsToDisplay
    });

    setTimeout(() => {
        dispatch({type: SET_ALERT, payload: []})
    }, timeout);
};
