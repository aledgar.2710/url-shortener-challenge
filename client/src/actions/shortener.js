import {CREATE_URL, SET_ORIGINAL_URL, HIDE_MODAL_URL} from './types';
import {setAlert} from "./alert"
import {setLoading} from "./loading"
import axios from 'axios';

export const createUrl = (originalUrl) => async dispatch => {

    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };
    dispatch(setLoading());
    try{
        const body = JSON.stringify({completeUrl: originalUrl});

        const response = await axios.post('http://localhost:3000/api/url-shorten/create',
            body,
            config);
        console.log(response);
        if (response.status === 200) {

            const payload = {
                originalUrl: '',
                showModalUrl: true,
                urlCreated: response.data.url
            };

            dispatch({
                type: CREATE_URL,
                payload
            })
        }

    }catch (e) {
        if (!e.response) {
            dispatch(setAlert(['We are in maintenance try later 🔧'], 6000));
        } else {
            dispatch(setAlert(e.response.data.errors));
        }
    }
    dispatch(setLoading());
};


export const setOriginalUrl = (originalUrl) => dispatch => {
    dispatch({
        type:SET_ORIGINAL_URL,
        payload: {
            originalUrl
        }
    })
};

export const displayModalUrl = () => dispatch => {
    dispatch({
        type:HIDE_MODAL_URL,
        payload: {
            showModalUrl: false,
            urlCreated:{}
        }
    });
};
