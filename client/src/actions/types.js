export const SET_LOADING = 'SET_LOADING';
export const SET_ALERT = 'SET_ALERT';
export const CREATE_URL = 'CREATE_URL';
export const SET_ORIGINAL_URL = 'SET_ORIGINAL_URL';
export const HIDE_MODAL_URL = 'DISPLAY_MODAL_URL';
export const GET_URLS = "GET_URLS";
export const DELETE_URL = "DELETE_URL";
