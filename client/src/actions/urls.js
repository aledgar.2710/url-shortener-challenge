import {GET_URLS, DELETE_URL} from './types';
import {setAlert} from "./alert"
import {setLoading} from "./loading"
import axios from 'axios';

const config = {
    headers: {
        'Content-Type': 'application/json'
    }
};

export const getUrls = () => async dispatch => {
    dispatch(setLoading());
    try {

        const response = await axios.get("http://localhost:3000/api/url-shorten/all",
            config);
        console.log(response);
        if (response.status === 200) {
            dispatch({
                type: GET_URLS,
                payload: response.data.urls
            })
        }

    } catch (e) {
        if (!e.response) {
            dispatch(setAlert(['We are in maintenance try later 🔧'], 6000));
        } else {
            dispatch(setAlert(e.response.data.errors));
        }
    }
    dispatch(setLoading());
};

export const deleteUrl = (urlCode) => async dispatch => {
    dispatch(setLoading());
    try{
        const response = await axios.delete(`http://localhost:3000/api/url-shorten/${urlCode}`,
            config);
        console.log(response);
        if (response.status === 200) {
            dispatch({
                type: DELETE_URL,
                payload: {
                    urlCode
                }
            })
        }
    }catch (e) {
        if (!e.response) {
            dispatch(setAlert(['We are in maintenance try later 🔧'], 6000));
        } else {
            dispatch(setAlert(e.response.data.errors));
        }
    }
    dispatch(setLoading());
};
