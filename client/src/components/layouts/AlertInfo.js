import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Alert} from "react-bootstrap";

const AlertInfo = ({errors}) =>
    errors !== null &&
    errors.length > 0 && (
    <Alert className={'alert-bg fadeIn animated'}>
        <ul>
            {errors.map(err => (
                <li key={err}>
                    {err.error}
                </li>
            ))};
        </ul>
    </Alert>);

AlertInfo.propTypes = {
    errors: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
    errors: state.alert
});

export default connect(mapStateToProps)(AlertInfo);
