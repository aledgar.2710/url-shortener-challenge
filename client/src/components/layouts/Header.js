import React from "react";
import {Link} from "react-router-dom";
import {Navbar, Nav} from "react-bootstrap"

const Header = () => (
    <Navbar className={'header-bg'} variant="dark">
        <Navbar.Brand href="#home">Url Shortener</Navbar.Brand>
        <Nav className="mr-auto">
            <Link className={'color-link mr-2'} to={'/shortener'}>Shortener</Link>
            <Link className={'color-link mr-2'} to={'/list-urls'}>Urls</Link>
        </Nav>
    </Navbar>
);


export default Header;
