import React, {useState} from "react";
import PropTypes from "prop-types";
import {Modal, Button} from "react-bootstrap";

const ModalUrl = ({show, handleClose, urlCreated: {shortUrl, completeUrl}}) => {

    const [copied, setCopied] = useState(false);

    const copyToClipboard = () =>{
        navigator.clipboard.writeText(shortUrl);
        setCopied(true);
        setTimeout(()=>{
            setCopied(false);
        },1500);
    };

    const cutOrginalUrl = () =>{
        if(completeUrl.length > 50){
            return completeUrl.substring(1,30)+"...";
        }else{
            return completeUrl;
        }
    };

    return (
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>You got the new url</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>Orignal url: <p style={{textAlign:"justify"}}>{cutOrginalUrl()}</p></p>
                <p>Short url: {shortUrl}</p>
                {copied && <p>The url has been copied 💪</p>}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="danger" onClick={handleClose}>
                    Close
                </Button>
                <Button variant="success" onClick={()=>copyToClipboard()}>
                    Copy to clipboard 📋
                </Button>
            </Modal.Footer>
        </Modal>
    )
};

ModalUrl.propTypes = {
    show: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    shortUrl: PropTypes.string.isRequired,
    completeUrl: PropTypes.string.isRequired
};

export default ModalUrl;
