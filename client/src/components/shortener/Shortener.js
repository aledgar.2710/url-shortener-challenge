import React, {Fragment, useState} from "react";
import {Button, Col, Row} from "react-bootstrap";
import {connect} from "react-redux";
import {setAlert} from "../../actions/alert";
import {createUrl, setOriginalUrl, displayModalUrl} from "../../actions/shortener";
import AlertInfo from "../layouts/AlertInfo";
import ModalUrl from "./ModalUrl";
import loading from "../../reducers/loading";

const Shortener = ({setAlert, createUrl, setOriginalUrl, displayModalUrl,
                       shortener: {originalUrl, showModalUrl, urlCreated}, loading}) => {

    const onChange = e => setOriginalUrl(e.target.value);
    const handleClose = () => displayModalUrl();

    const generateShortUrl = () => {

        if (originalUrl === "") {
            setAlert(['Please check your link and try again.']);
        } else {
            createUrl(originalUrl);
        }
    };

    return (
        <Fragment>
            <Row className={'text-center'} style={{marginTop: 100}}>
                <Col className={"shadow p-3"} md="12">
                    <p className={'font-weight-bold'}>Shortener</p>
                    <div className={'d-flex flex-row'}>
                        <input value={originalUrl} onChange={(e) => onChange(e)} type={'text'}
                               className={'form-control mr-1'}
                               placeholder={'Shorten your link -> https://es.reactjs.org'}/>
                        <Button onClick={generateShortUrl} size="sm" style={{width: 150}} className={'button-bg'}>
                            {(loading) ? 'loading ... 🙊' : 'Shorten ✂' }
                        </Button>
                    </div>
                    <div className={'mt-2'}>
                        <AlertInfo/>
                    </div>
                </Col>
            </Row>
            {showModalUrl && <ModalUrl show={showModalUrl} handleClose={handleClose} urlCreated={urlCreated}/>}
        </Fragment>
    );
};

const mapStateToProps = state => ({
    shortener: state.shortener,
    loading: state.loading
});

export default connect(mapStateToProps, {setAlert, createUrl, setOriginalUrl, displayModalUrl})(Shortener);
