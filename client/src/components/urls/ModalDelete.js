import React from "react";
import {Button, Modal} from "react-bootstrap";
import PropTypes from "prop-types";

const ModalDelete = ({show, handleClose, confirmDelete, loading}) => {
    return (
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Delete the URL</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>Are you sure to delete this URL?</p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Close
                </Button>
                <Button variant="danger" onClick={confirmDelete}>
                    {(loading)? 'loading... 🙊' : 'Delete'}
                </Button>
            </Modal.Footer>
        </Modal>
    )
};

ModalDelete.propTypes = {
    show: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    confirmDelete: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired
};

export default ModalDelete;
