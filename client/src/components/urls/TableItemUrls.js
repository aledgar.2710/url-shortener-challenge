import React from "react";
import {Button} from "react-bootstrap";
import PropTypes from "prop-types";

const TableItemUrls = ({url, delUrl}) => {
    const cutOrginalUrl = (originalUrl) => {
        if (originalUrl.length > 80) {
            return originalUrl.substring(1, 50) + "...";
        } else {
            return originalUrl;
        }
    };

    return (
        <tr key={url.urlCode}>
            <td>{cutOrginalUrl(url.completeUrl)}</td>
            <td><a href={url.shortUrl} target={'_blank'}>{url.shortUrl}</a></td>
            <td><Button onClick={()=>delUrl(url.urlCode)} variant="outline-danger">Delete</Button></td>
        </tr>
    )
};

TableItemUrls.propTypes = {
    url: PropTypes.object.isRequired,
    delUrl: PropTypes.func.isRequired,
};

export default TableItemUrls;
