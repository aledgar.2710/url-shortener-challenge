import React from 'react';
import {Table} from "react-bootstrap";
import PropTypes from "prop-types";
import TableItemUrls from "./TableItemUrls";

const TableUrls = ({urls, delUrl}) => (
    <Table striped bordered size="sm">
        <thead>
        <tr>
            <th>Orginal url</th>
            <th>Short url</th>
            <th>Options</th>
        </tr>
        </thead>
        <tbody>
        {
            urls.map((url,index) =>
                <TableItemUrls key={'titem-'+index} url={url} delUrl={delUrl} />
            )
        }
        </tbody>
    </Table>
);

TableUrls.propTypes = {
    urls: PropTypes.array.isRequired,
    delUrl: PropTypes.func.isRequired,
};

export default TableUrls;
