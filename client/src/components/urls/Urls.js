import React, {useEffect, useState, Fragment} from "react";
import {Row, Col} from "react-bootstrap";
import {connect} from "react-redux";
import {setAlert} from "../../actions/alert";
import {getUrls, deleteUrl} from "../../actions/urls";
import TableUrls from "./TableUrls";
import AlertInfo from "../layouts/AlertInfo";
import ModalDelete from "./ModalDelete";

const Urls = ({getUrls, urls, loading, deleteUrl}) => {

    const [showModalDelete, setShowModalDelte] = useState(false);
    const [urlCodeDelete, setUrlCodeDelete] = useState("");

    useEffect(() => {
        getUrls();
    }, []);

    const setHandleCloseModalDel = () => {
        setShowModalDelte(false);
        setUrlCodeDelete("");
    };

    const delUrl = (urlCode) => {
        setShowModalDelte(true);
        setUrlCodeDelete(urlCode);
    };

    const confirmDelete = () => {
        deleteUrl(urlCodeDelete);
        setShowModalDelte(false);
        setUrlCodeDelete("");
    };

    return (
        <Fragment>
            {loading ?
                <p>loading... 🙊</p>
                : (
                    <Fragment>
                        <Row style={{marginTop: 10}}>
                            <Col md="12">
                                <AlertInfo/>
                            </Col>
                        </Row>
                        <Row className={'text-center'} style={{marginTop: 40, marginBottom: 40}}>
                            <Col className={'table-responsive shadow p-3'} md="12">
                                <h4>URLS Generated</h4>
                                {(urls.length > 0) ?
                                    <TableUrls urls={urls} delUrl={delUrl}/>
                                    : <p>there were not urls in our records 🙂</p>}
                            </Col>
                        </Row>
                    </Fragment>
                )
            }
            <ModalDelete show={showModalDelete} handleClose={setHandleCloseModalDel} loading={loading} confirmDelete={confirmDelete}/>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    urls: state.urls,
    loading: state.loading
});

export default connect(mapStateToProps, {setAlert, getUrls, deleteUrl})(Urls);
