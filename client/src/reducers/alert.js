import {SET_ALERT, REMOVE_ALERT} from "../actions/types";

const intialState = [];

export default function (state = intialState, action) {

    const {type, payload} = action;

    switch (type) {
        case SET_ALERT:
            return payload;
        default:
            return state;
    }

}
