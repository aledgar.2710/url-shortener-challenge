import {combineReducers} from 'redux';
import loading from './loading';
import alert from "./alert";
import shortener from "./shortener";
import urls from "./urls";

export default combineReducers({
    loading,
    alert,
    shortener,
    urls
});
