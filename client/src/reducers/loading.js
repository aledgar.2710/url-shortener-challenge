import {SET_LOADING} from "../actions/types";

const initialState = false;

export default function (state = initialState, action){
    const {type} = action;

    switch (type) {
        case SET_LOADING:
            return state = !state;
        default:
            return state;
    }

}
