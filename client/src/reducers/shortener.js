import {CREATE_URL, SET_ORIGINAL_URL, DISPLAY_MODAL_URL, HIDE_MODAL_URL} from "../actions/types";

const initialState = {
    originalUrl:'',
    showModalUrl: false,
    urlCreated: {}
};

export default function (state = initialState, action){
    const {type, payload} = action;

    switch (type) {
        case CREATE_URL:
            return {
                ...payload
            };
        case SET_ORIGINAL_URL:
        case HIDE_MODAL_URL: return {...state,...payload};
        default:
            return state;
    }

}
