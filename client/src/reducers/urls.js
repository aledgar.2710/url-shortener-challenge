import {GET_URLS,DELETE_URL} from "../actions/types";

const initialState = [];

export default function (state = initialState, action) {
    const {type, payload} = action;
    switch (type) {
        case GET_URLS:
            return [...payload];
        case DELETE_URL:
            return state.filter(u => u.urlCode !== payload.urlCode);
        default:
            return state;
    }

}
