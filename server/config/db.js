const mongoose = require('mongoose');
require('dotenv').config();

const connectDB = async () => {
    /*const connectionString = `mongodb://${process.env.MONGO_DB_USER}:${process.env.MONGO_DB_PASS}@
            ${process.env.MONGO_DB_HOST}:${process.env.MONGO_DB_PORT}/${process.env.MONGO_DB_NAME}/${process.env.MONGO_DB_AUTH}`;*/
    const connectionString = `mongodb://${process.env.MONGO_DB_HOST}:${process.env.MONGO_DB_PORT}/shortener`;
    try {

        await mongoose.connect(connectionString, {
                useNewUrlParser: true
            });
        console.log('connected to the db');
    } catch (e) {
        console.log(e.message);
        process.exit(1);
    }
};

module.exports = connectDB;
