const shortid = require('shortid');
const UrlShorten = require('../models/UrlShorten');
const validUrl = require('valid-url');
require("dotenv").config();


// @route    GET /:code
// @desc     GET specific url and redirect to the associate website
// @access   Public
exports.getUrl = async (req, res) => {
    const {code} = req.params;
    try {
        const url = await UrlShorten.findOne({urlCode: code});
        if (url) {
            return res.redirect(url.completeUrl);
        } else {
            return res.status(404).json({success: false, msg: 'URL not found'})
        }
    } catch (e) {
        console.error(e);
        return res.status(500).json({success: false, errors: ['We have some troubles, if this continue, please get in touch with technical support']});
    }
};

// @route    POST api/url-shorten/create
// @desc     POST create a new short URL
// @access   Public
exports.createUrl = async (req, res) => {

    const {completeUrl} = req.body;
    const baseUrl = process.env.DOMAIN_HOST+':'+process.env.PORT;

    if (validUrl.isUri(completeUrl)) {
        try {

            let url = await UrlShorten.findOne({completeUrl});

            if (url) {
                return res.json({success: true, url})
            } else {
                const urlCode = shortid.generate();
                const shortUrl = baseUrl + '/' + urlCode;
                url = new UrlShorten({
                    completeUrl,
                    shortUrl,
                    urlCode,
                    date: new Date()
                });
                await url.save();
                return res.json({success: true, url});
            }
        } catch (e) {
            console.error(e);
            return res.status(500).json({
                success: false,
                errors: ['We have some troubles, if this continue, please get in touch with technical support']
            });
        }
    }else{
        res.status(401).json({errors:['Invalid long url, please add http:// or https:// at the beginning of the URL']});
    }
};

// @route    GET api/url-shorten/all
// @desc     Get all the urls from the db
// @access   Public
exports.getAllUrls = async (req,res) => {

    try{
        const urls = await UrlShorten.find({}).select("-__v");
        return res.json({success:true, urls})
    }catch (e) {
        console.error(e);
        return res.status(500).json({success: false, errors: ['We have some troubles, if this continue, please get in touch with technical support']});
    }
};

// @route    DELETE api/url-shorten/:code
// @desc     Delete url
// @access   Public
exports.deleteUrl = async (req,res) =>{
    try{
        const foundUrl = await UrlShorten.findOne({urlCode: req.params.code});

        if(foundUrl){
            await UrlShorten.findOneAndRemove({urlCode: req.params.code});
            return res.json({success:true, msg: 'URL deleted'});
        }else{
            return res.status(404).json({errors:['The url was not found']});
        }
    }catch (e) {
        console.error(e);
        return res.status(500).json({success: false, errors: ['We have some troubles, if this continue, please get in touch with technical support']});
    }
};

