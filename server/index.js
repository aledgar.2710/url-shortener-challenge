const express = require('express');
const connectDB = require('./config/db');
const bodyParser = require('body-parser');
const routes = require('./routes/index');
require("dotenv").config();

const app = express();
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//Connecting to the database
connectDB();

//Configuring the cors
app.use((req, res, next) => {
   res.header('Access-Control-Allow-Origin', '*');
   res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
   res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
   res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
   next();
});

//Configuring the routes
app.use("/", routes());

app.listen(process.env.PORT, process.env.DOMAIN_HOST,()=>{
   console.log('Connected on port ', process.env.PORT);
});
