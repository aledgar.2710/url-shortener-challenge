const mongoose = require('mongoose');
const {Schema} = mongoose;

const urlShortenSchema = new Schema({
    completeUrl: {
        type: String,
        required: true
    },
    shortUrl: {
        type:String,
        required: true
    },
    urlCode:{
      type: String,
      required: true
    },
    createdAt: {type: Date, default: Date.now}
});

module.exports = mongoose.model("UrlShorten", urlShortenSchema);
