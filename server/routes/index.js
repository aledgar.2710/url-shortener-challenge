const express = require('express');
const router = express.Router();

const urlShortenController = require('../controllers/urlShortenController');

module.exports = function () {
    router.get('/:code', urlShortenController.getUrl);
    router.post('/api/url-shorten/create', urlShortenController.createUrl);
    router.get('/api/url-shorten/all', urlShortenController.getAllUrls);
    router.delete('/api/url-shorten/:code',urlShortenController.deleteUrl);
    return router;
};

